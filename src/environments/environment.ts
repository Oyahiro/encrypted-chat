// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false
};

export const firebaseConfig = {
  apiKey: "AIzaSyALZjs2recne4jw48v9zx23-2rDgh58Z90",
  authDomain: "encrypted-chat-12119.firebaseapp.com",
  databaseURL: "https://encrypted-chat-12119.firebaseio.com",
  projectId: "encrypted-chat-12119",
  storageBucket: "encrypted-chat-12119.appspot.com",
  messagingSenderId: "161920532334",
  appId: "1:161920532334:web:4749beab32add6c05fcb7f"
};

//Develop
export const API_URL = 'http://localhost:3000';

// Produccion
// export const API_URL = 'https://espe-security.herokuapp.com';

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
