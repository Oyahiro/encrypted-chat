import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';
import { UserService } from '../../services/user.service';
import { MailboxService } from '../../services/mailbox.service';
import { User } from './../../models/User';
import { SecretMessage } from './../../models/SecretMessage';
import { ActionSheetController } from '@ionic/angular';
import { ModalController } from '@ionic/angular';
import { SecretMessageComponent } from '../../components/secret-message/secret-message.component';
import { SendSecretMessageComponent } from '../../components/send-secret-message/send-secret-message.component';

@Component({
  selector: 'app-secret-mailbox',
  templateUrl: './secret-mailbox.page.html',
  styleUrls: ['./secret-mailbox.page.scss'],
})
export class SecretMailboxPage implements OnInit {

  readonly icon = 'https://firebasestorage.googleapis.com/v0/b/encrypted-chat-12119.appspot.com/o/proteger.png?alt=media&token=35b577a0-1069-4e56-a8b2-928d7d2e6694';
  messages: any = [];
  logged_user:User;

  constructor(private router:Router,
    private userService:UserService,
    private mailboxService:MailboxService,
    public actionSheetController: ActionSheetController,
    private modal:ModalController,) { }

  ngOnInit() {
    this.mailboxService.getSecretMessages()
      .subscribe(messages => {   
        this.logged_user = JSON.parse(localStorage.getItem('user')) as User;
        this.messages = messages;
        this.filterMessages();
      });
  }

  openTemporalMessage(message) {
    this.modal.create({
      component:SecretMessageComponent,
      componentProps: {
        message: message
      }
    }).then((modal) => {
      modal.present()
    })
  }

  filterMessages(){
    // Busca los indices de los usuarios no disponibles
    var deleteIndex: string[] = [];
    for(let message of this.messages) {
      var messageAct = message as SecretMessage;
      if(messageAct.user_uid!==this.logged_user.uid){
        var i = this.messages.indexOf(messageAct);
        deleteIndex.push(i);
      }
    }

    // Elimina usuarios no disponibles del arreglo
    deleteIndex = deleteIndex.reverse();
    for(let i of deleteIndex) {
      this.messages.splice(i, 1);
    }
  }

  openMessageModal() {
    this.modal.create({
      component:SendSecretMessageComponent,
      componentProps: {
        // message: message
      }
    }).then((modal) => {
      modal.present()
    })
  }

  returnBack(){
    this.router.navigate(['/home']);
  }
}
