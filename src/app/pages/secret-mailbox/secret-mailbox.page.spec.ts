import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SecretMailboxPage } from './secret-mailbox.page';

describe('SecretMailboxPage', () => {
  let component: SecretMailboxPage;
  let fixture: ComponentFixture<SecretMailboxPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SecretMailboxPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SecretMailboxPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
