import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SecretMailboxPage } from './secret-mailbox.page';

const routes: Routes = [
  {
    path: '',
    component: SecretMailboxPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SecretMailboxPageRoutingModule {}
