import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { SecretMailboxPage } from './secret-mailbox.page';

import { SecretMailboxPageRoutingModule } from './secret-mailbox-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SecretMailboxPageRoutingModule
  ],
  declarations: [SecretMailboxPage]
})
export class SecretMailboxPageModule {}
