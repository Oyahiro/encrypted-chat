import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { SecurityService } from '../../services/security.service';
import { LoadingService } from '../../services/loading.service';
import { Router } from '@angular/router';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { ToastController } from '@ionic/angular';
import { User } from '../../models/User';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  user: User = {
    uid: '',
    name: '',
    state: '',
    avatar: '',
    RSA: {
      private_key: '',
      public_key: ''
    }
  };
  register: FormGroup;
  base_url:string = 'https://firebasestorage.googleapis.com/v0/b/encrypted-chat-12119.appspot.com/o/';
  
  constructor(private authService:AuthService,
    private securityService:SecurityService,
    private loadingService:LoadingService,
    private router:Router,
    private formBuilder: FormBuilder,
    public toastController: ToastController) {
      this.register = this.formBuilder.group({
        name: ['', [Validators.required]],
        state: ['', [Validators.required]],
        email: ['', [Validators.required, Validators.email]],
        password: ['', [Validators.required]],
        confirmPassword: ['', [Validators.required]],
      });
    }

  ngOnInit() {
  }

  async presentToast(message:string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }

  onSubmitRegister() {
    if(this.register.value.password !== this.register.value.confirmPassword) {
      this.presentToast('Las contraseñas no coinciden');
    }else if (this.user.avatar === '') {
      this.presentToast('Debe elegir un avatar');
    }else {
      this.loadingService.present();
      this.user.name = this.register.value.name;
      this.user.state = this.register.value.state;

      this.securityService.generateRSAKeys()
        .subscribe(res => { 
          let response = res as any;
          this.user.RSA.private_key = response.private;
          this.user.RSA.public_key = response.public;
          this.registerUser();
        });
    }
  }

  registerUser() {
    this.authService.register(this.register.value.email, this.register.value.password, this.user)
      .then(auth => {
        this.cleanForm();
        this.loadingService.dismiss();
        this.router.navigate(['/home']);
      }).catch(err => {
        if(err.code==='auth/email-already-in-use')
          this.presentToast('El correo ya esta en uso');
        console.log(err);
      });
  }

  selectAvatar(name:string) {
    this.user.avatar = this.base_url + name;
  }

  cleanForm() {
    this.user.uid = '';
    this.user.avatar = '';
    this.register.reset();
  }
}
