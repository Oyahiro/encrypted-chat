import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router} from '@angular/router';
import { ToastController } from '@ionic/angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  email:string;
  password:string;
  login: FormGroup;

  constructor(private authService:AuthService,
    public router:Router,
    private formBuilder: FormBuilder,
    public toastController: ToastController) {
      this.login = this.formBuilder.group({
        email: ['', [Validators.required, Validators.email]],
        password: ['', [Validators.required]]
      });
     }

  ngOnInit() {
  }

  async presentToast(message:string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }

  onSubmitLogin() {
    this.authService.login(this.login.value.email, this.login.value.password)
      .then(res => {
        this.login.reset();
        this.router.navigate(['/home']);
      })
      .catch(err => {
        this.presentToast('Error al iniciar sesion');
      });
  }
}
