import { Component } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { ChatService } from '../../services/chat.service';
import { ModalController } from '@ionic/angular';
import { ChatComponent }  from '../../components/chat/chat.component';
import { UsersComponent } from '../../components/users/users.component';
import { ActionSheetController } from '@ionic/angular';
import { Router} from '@angular/router';
import { ChatRoom } from './../../models/ChatRoom';
import { User } from '../../models/User';
import { UserData } from '../../models/ChatRoom';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  chatRooms:any = [];
  chatRoomsIds: string[] = [];
  user:User;
  creator_user:UserData = {
    uid: '',
    name: '',
    avatar: '',
    state: ''
  };

  constructor(private authService:AuthService,
    private chatService:ChatService,
    private modal:ModalController,
    public router:Router,
    public actionSheetController: ActionSheetController) {}

  ngOnInit() {
    this.chatService.getChatRooms()
      .subscribe(chats => {    
        this.user = JSON.parse(localStorage.getItem('user')) as User;
        this.chatRooms = chats;
        this.filterChatRooms();
      })
  }

  // Filtra las conversaciones a solo en las que esta el usuario logueado
  filterChatRooms(){
    var deleteIndex: string[] = [];
    this.chatRoomsIds.push(this.user.uid);
    for(let chatRoom of this.chatRooms) {
      var room = chatRoom as ChatRoom;
      if(room.creator_user.uid!==this.user.uid && room.host_user.uid!==this.user.uid){
        var i = this.chatRooms.indexOf(chatRoom);
        deleteIndex.push(i);
      } else {
        if(room.creator_user.uid!==this.user.uid)
          this.chatRoomsIds.push(room.creator_user.uid);
        else
          this.chatRoomsIds.push(room.host_user.uid);
      }
    }
    deleteIndex = deleteIndex.reverse();
    for(let i of deleteIndex) {
      this.chatRooms.splice(i, 1);
    }
  }

  openChat(chat) {
    this.modal.create({
      component:ChatComponent,
      componentProps: {
        chat: chat
      }
    }).then((modal) => {
      modal.present()
    })
  }

  startChat() {
    this.assignCreatorData();
    this.modal.create({
      component:UsersComponent,
      componentProps: {
        creator_user: this.creator_user,
        disabledUsers: this.chatRoomsIds,
        public_key: this.user.RSA.public_key
      }
    }).then((modal) => {
      modal.present()
    });
  }

  onLogOut() {
    this.authService.logout();
  }

  assignCreatorData() {
    this.creator_user.name = this.user.name;
    this.creator_user.state = this.user.state;
    this.creator_user.avatar = this.user.avatar;
    this.creator_user.uid = this.user.uid;
  }

  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Opciones',
      
      buttons: [{
        text: 'Iniciar chat',
        icon: 'chatbubbles-outline',
        handler: () => {
          this.startChat();
        }
      },{
        text: 'Buzón temporal',
        icon: 'lock-closed-outline',
        handler: () => {
          this.router.navigate(['/secret-mailbox']);
        }
      },{
        text: 'Desconectarse',
        role: 'destructive',
        icon: 'log-out-outline',
        handler: () => {
          this.onLogOut();
        }
      },
    ]
    });
    await actionSheet.present();
  }
}
