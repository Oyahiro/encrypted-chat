import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';
import { UserService } from '../../services/user.service';
import { ChatService } from '../../services/chat.service';
import { SecurityService } from '../../services/security.service';
import { UserData, ChatRoom, ChatKeys } from '../../models/ChatRoom';
import { User } from './../../models/User';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
})
export class UsersComponent implements OnInit {

  users: any = [];
  disabledUsers: string[] = [];

  public_key_looged_user:string
  creator_user:UserData;
  host_user:UserData;
  hashKey:string;
  chatRoom:ChatRoom;
  keys:ChatKeys = {
    creator_code: '',
    host_code: ''
  };

  constructor(private navParams:NavParams,
    private modal:ModalController,
    private userService:UserService,
    private chatService:ChatService,
    private securityService:SecurityService) { }

  ngOnInit() {
    // Captura los atributos enviados al modal
    this.creator_user = this.navParams.get('creator_user');
    this.disabledUsers = this.navParams.get('disabledUsers');
    this.public_key_looged_user = this.navParams.get('public_key');

    // Consulta todos los usuarios
    this.userService.getUsers()
      .subscribe(users => {    
        this.users = users;
        this.filterUsers();
      });
  }

  filterUsers(){
    // Busca los indices de los usuarios no disponibles
    var deleteIndex: string[] = [];
    for(let user of this.users) {
      var userAct = user as User;
      if(this.disabledUsers.includes(userAct.uid)){
        var i = this.users.indexOf(user);
        deleteIndex.push(i);
      }
    }

    // Elimina usuarios no disponibles del arreglo
    deleteIndex = deleteIndex.reverse();
    for(let i of deleteIndex) {
      this.users.splice(i, 1);
    }
  }

  createChatRoom(user) {
    this.securityService.generateHashKey()
      .subscribe(res => {
        let response = res as any;
        this.hashKey = response.hashKey;
        
        // Encriptar clave hash con RSA para usuario actual
        this.securityService.encryptWithRSA(this.public_key_looged_user, this.hashKey.toString())
          .subscribe(res => {
            let response = res as any;
            this.keys.creator_code = response.encryptedText;
            
            // Encriptar clave hash con RSA para usuario amigo
            this.securityService.encryptWithRSA(user.RSA.public_key, this.hashKey.toString())
              .subscribe(res => {
                let response = res as any;
                this.keys.host_code = response.encryptedText;

                // Guardar sala de chat en Firebase
                delete user.RSA;
                this.host_user = user;
                this.chatService.createChatRoom(this.creator_user, this.host_user, this.keys);
                this.closeModal();
              });
          });
      })
    
  }

  closeModal() {
    this.modal.dismiss();
  }
}
