import { Component, OnInit, ViewChild } from '@angular/core';
import { NavParams, ModalController, IonContent } from '@ionic/angular';
import { ChatService } from '../../services/chat.service';
import { SecurityService } from '../../services/security.service';
import { ChatRoom } from './../../models/ChatRoom';
import { Message } from '../../models/Message';
import { User } from '../../models/User';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss'],
})
export class ChatComponent implements OnInit {

  @ViewChild(IonContent) content:IonContent;

  messages = [ ];
  contentMessage:string;
  chatRoom:any;
  param_chat:ChatRoom;
  logged_user:User;
  friend_user:User;
  hashKey:string;

  constructor(private chatService:ChatService,
    private securityService:SecurityService,
    private navParams:NavParams,
    private modal:ModalController) {  }

  ngOnInit() {
    this.param_chat = this.navParams.get('chat');
    this.logged_user = JSON.parse(localStorage.getItem('user')) as User;

    // Obtiene todas las salas de chat de la base de datos
    this.chatService.getChatRoom(this.param_chat.uid)
      .subscribe(room => {
        this.chatRoom = room;
        
        // Obtener y desencriptar clave de sala
        let keyRoom:string = '';
        if(this.chatRoom.creator_user.uid===this.logged_user.uid)
          keyRoom = this.chatRoom.keys.creator_code;
        else 
          keyRoom = this.chatRoom.keys.host_code;

        this.securityService.decryptWithRSA(this.logged_user.RSA.private_key, keyRoom)
          .subscribe(res => {
            let response = res as any;
            this.hashKey = response.decryptedText;

            if(this.chatRoom.messages)
              this.securityService.decryptAllMessages(this.chatRoom.messages, this.hashKey);
            
            this.scrollToBottomChat();
          });
      });
  }

  sendMessage() {
    // Encripta el mensaje antes de enviarlo
    this.securityService.encryptWithAES(this.contentMessage, this.hashKey)
      .subscribe(res => {
        let response = res as any;

        // Se crea el objeto "Message" que se adicionará a la colección ChatRoom en firebase
        const message:Message = {
          content : response.message,
          date : new Date(),
          user_uid: this.logged_user.uid
        }

        this.chatService.sendMessageToFirebase(message, this.param_chat.uid);
        this.contentMessage = '';

        this.scrollToBottomChat();
      });
  }

  closeChat() {
    this.modal.dismiss();
  }

  scrollToBottomChat() {
    setTimeout(() => {
      this.content.scrollToBottom(200);
    }, 100);
  }
}
