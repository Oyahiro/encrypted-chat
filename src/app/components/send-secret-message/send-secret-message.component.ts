import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { UserService } from '../../services/user.service';
import { MailboxService } from '../../services/mailbox.service';
import { SecurityService } from '../../services/security.service';
import { User } from '../../models/User';
import { SecretMessage } from '../../models/SecretMessage';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-send-secret-message',
  templateUrl: './send-secret-message.component.html',
  styleUrls: ['./send-secret-message.component.scss'],
})
export class SendSecretMessageComponent implements OnInit {

  logged_user:User;
  users: any = [];
  secretMessage:string;
  userToSend:User = {
    uid: '',
    avatar: '',
    state: '',
    name: '',
    RSA: null
  };

  constructor(private modal:ModalController,
    private userService:UserService,
    private securityService:SecurityService,
    private mailboxService:MailboxService,
    public toastController: ToastController) { }

  ngOnInit() {
    this.userService.getUsers()
      .subscribe(users => {    
        this.logged_user = JSON.parse(localStorage.getItem('user')) as User;
        this.users = users;
        this.filterUsers();
      });
  }

  filterUsers(){
    for(let user of this.users) {
      var userAct = user as User;
      if(userAct.uid===this.logged_user.uid){
        var i = this.users.indexOf(user);
        this.users.splice(i, 1);
        break;
      }
    }
  }

  sendMessage() {
    if(this.secretMessage!==''){
      if(this.userToSend.uid!==''){
        // Encripta el mensaje antes de enviarlo
        this.securityService.encryptWithRSA(this.userToSend.RSA.public_key, this.secretMessage)
          .subscribe(res => {
            let response = res as any;

            // Se crea el objeto "Message" que se adicionará a la colección ChatRoom en firebase
            const messageToSend:SecretMessage = {
              user_uid: this.userToSend.uid,
              content: response.encryptedText,
              date: new Date(),
              name_user_send: this.logged_user.name
            }

            this.mailboxService.sendSecretMessage(messageToSend);
            this.closeModal();
          });
      }else {
        this.presentToast('Selecciona un destinatario de la lista');
      }
    }else {
      this.presentToast('El mensaje no puede estar vacío');
    }
  }

  async presentToast(message:string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000
    });
    toast.present();
  }

  assignUserName(user:User) {
    this.userToSend = user;
  }

  closeModal() {
    this.modal.dismiss();
  }
}
