import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController, IonContent } from '@ionic/angular';
import { User } from '../../models/User';
import { SecretMessage } from '../../models/SecretMessage';
import { SecurityService } from '../../services/security.service';
import { MailboxService } from '../../services/mailbox.service';

@Component({
  selector: 'app-secret-message',
  templateUrl: './secret-message.component.html',
  styleUrls: ['./secret-message.component.scss'],
})
export class SecretMessageComponent implements OnInit {

  logged_user:User;
  show:boolean = false;
  param_temporal_message:any;

  constructor(private navParams:NavParams,
    private modal:ModalController,
    private securityService:SecurityService,
    private maiboxService:MailboxService) { }

  ngOnInit() {
    this.param_temporal_message = this.navParams.get('message');
    this.logged_user = JSON.parse(localStorage.getItem('user')) as User;
    this.securityService.decryptWithRSA(this.logged_user.RSA.private_key, this.param_temporal_message.content)
      .subscribe(res => {
        let response = res as any;
        this.param_temporal_message.content = response.decryptedText;
        // console.log(this.param_temporal_message.date.getMilliseconds);
      });
  }

  seeSecretMessage() {
    this.show = true;
    setTimeout(() => {
      this.show = false;
      this.maiboxService.deleteSecretMessage(this.param_temporal_message.uid);
      this.closeModal();
    }, 5000)
  }

  closeModal() {
    this.modal.dismiss();
  }
}
