import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Message } from '../models/Message';
import { ChatRoom, UserData, ChatKeys } from '../models/ChatRoom';
import { map } from 'rxjs/operators';
import { firestore } from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  constructor(private db:AngularFirestore) { }

  getChatRooms(){
    return this.db.collection('chatRooms')
      .snapshotChanges()
      .pipe(map(rooms => {
        return rooms.map(a => {
          const data = a.payload.doc.data() as ChatRoom;
          data.uid = a.payload.doc.id;
          return data;
        })
      }));
  }

  getChatRoom(chat_id:string) {
    return this.db.collection('chatRooms')
      .doc(chat_id)
      .valueChanges();
  }

  createChatRoom(creator:UserData, host:UserData, keys:ChatKeys) {
    this.db.collection('chatRooms')
      .add({
        creator_user: creator,
        host_user: host,
        keys: keys
      });
  }

  sendMessageToFirebase(message:Message, chat_id:string) {
    this.db.collection('chatRooms')
      .doc(chat_id)
      .update({
        messages: firestore.FieldValue.arrayUnion(message)
      })
  }
}
