import { Injectable } from '@angular/core';
import { AngularFireAuth} from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { User } from './../models/User';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private auth: AngularFireAuth,
    public router:Router,
    private db:AngularFirestore) { }

  register(email:string, password:string, user: User) {
    return new Promise((resolve, reject) => {
      this.auth.createUserWithEmailAndPassword(email, password)
        .then(res => {
          const uid = res.user.uid;
          this.db.collection('users')
            .doc(uid)
            .set({
              name: user.name,
              avatar: user.avatar,
              state: user.state,
              RSA: {
                private_key: user.RSA.private_key,
                public_key: user.RSA.public_key
              }
            })
          this.setLocalUser(uid);
          resolve(res)
        }).catch(err => {
          reject(err)
        });
    })
  }

  login(email:string, password:string) {
    return new Promise((resolve, rejected) => {
      this.auth.signInWithEmailAndPassword(email, password)
        .then(user => {
          this.setLocalUser(user.user.uid);
          resolve(user);
        })
        .catch(err => {
          rejected(err)
        })
    });
  }

  logout() {
    this.auth.signOut()
      .then(auth => {
        localStorage.removeItem('user');
        this.router.navigate(['/login']);
      });
  }

  setLocalUser(uid:string) {
    this.db.collection('users').doc(uid).valueChanges().subscribe(data => {
      var user = data as User;
      user.uid = uid;
      localStorage.setItem('user', JSON.stringify(user));
    })
  }
}
