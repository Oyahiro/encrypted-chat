import { Injectable } from '@angular/core';
import { API_URL } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Message } from '../models/Message';

@Injectable({
  providedIn: 'root'
})
export class SecurityService {

  readonly rsa_route = API_URL + '/api/rsa';
  readonly hash_route = API_URL + '/api/hash';

  constructor(private http: HttpClient) { }

  generateHashKey() {
    return this.http.get(this.hash_route + '/generate');
  }

  encryptWithAES(messageToEncrypt:string, hashKey:string) {
    let body:any = {
      messageToEncrypt: messageToEncrypt,
      hashKey: hashKey
    }
    return this.http.post(this.hash_route + '/encrypt', body);
  }

  decryptWithAES(messageToDecrypt:string, hashKey:string) {
    let body:any = {
      messageToDecrypt: messageToDecrypt,
      hashKey: hashKey
    }
    return this.http.post(this.hash_route + '/decrypt', body);
  }

  generateRSAKeys() {
    return this.http.get(this.rsa_route + '/generate');
  }

  encryptWithRSA(rsaPublicKey:string, hashCode:string) {
    let body:any = {
      rsaPublicKey: rsaPublicKey,
      hashCode: hashCode
    }
    return this.http.post(this.rsa_route + '/encrypt', body);
  }

  decryptWithRSA(rsaPrivateKey:string, hashCode:string) {
    let body:any = {
      rsaPrivateKey: rsaPrivateKey,
      hashCode: hashCode
    }
    return this.http.post(this.rsa_route + '/decrypt', body);
  }

  decryptAllMessages(messages:Message[], hashKey:string){
    // Mapea todo el arreglo de mensales
    return messages.map(m => {
      // Desencripta por medio de AES
      this.decryptWithAES(m.content, hashKey)
        .subscribe(res => {
          let response = res as any;
          m.content = response.message;
          return m;
        });
    });
  }
}
