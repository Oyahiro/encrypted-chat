import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import { SecretMessage } from '../models/SecretMessage';

@Injectable({
  providedIn: 'root'
})
export class MailboxService {

  constructor(private db:AngularFirestore) { }

  getSecretMessages(){
    return this.db.collection('secretMessages')
      .snapshotChanges()
      .pipe(map(rooms => {
        return rooms.map(a => {
          const data = a.payload.doc.data() as SecretMessage;
          data.uid = a.payload.doc.id;
          return data;
        })
      }));
  }

  getSecretMessage(message_id:string) {
    return this.db.collection('secretMessages')
      .doc(message_id)
      .valueChanges();
  }

  sendSecretMessage(message:SecretMessage) {
    this.db.collection('secretMessages').add(message);
  }

  deleteSecretMessage(uid:string) {
    this.db.collection("secretMessages").doc(uid).delete()
      .then(function() {
        console.log("Documento temporal eliminado");
      }).catch(function(error) {
        console.error("Error al eliminar: ", error);
      });
  }
}
