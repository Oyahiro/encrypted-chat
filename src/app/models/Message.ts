export interface Message {
    content?: string;
    date?: Date;
    user_uid?: string;
}