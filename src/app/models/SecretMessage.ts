export interface SecretMessage {
    uid?:string;
    content?: string;
    date?: Date;
    user_uid?: string;
    name_user_send: string;
}