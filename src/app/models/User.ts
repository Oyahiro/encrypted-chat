interface Keys {
    private_key?: string;
    public_key?: string;
}

export interface User {
    uid:string;
    name:string;
    avatar:string;
    state:string;
    RSA: Keys;
  }