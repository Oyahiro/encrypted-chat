export interface UserData {
  uid?: string;
  name?: string;
  avatar?: string;
  state?: string;
}

export interface ChatKeys {
  creator_code?: string;
  host_code?: string;
}

export interface ChatRoom {
  uid?:string;
  creator_user?: UserData;
  host_user?: UserData;
  keys?: ChatKeys;
  messages?:any;
}